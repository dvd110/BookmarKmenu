package com.browser.bookmark.service;

import com.browser.bookmark.entity.Bookmark;
import com.browser.bookmark.entity.Fans;
import com.browser.bookmark.entity.FansExample;
import com.browser.bookmark.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Auther: Wang
 * @Date: 2020/02/17 22:34
 * 功能描述:
 */
public interface IFansService {
    long countByExample(FansExample example);

    int deleteByExample(FansExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Fans record);

    int insertSelective(Fans record);

    List<Fans> selectByExample(FansExample example);

    Fans selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Fans record, @Param("example") FansExample example);

    int updateByExample(@Param("record") Fans record, @Param("example") FansExample example);

    int updateByPrimaryKeySelective(Fans record);

    int updateByPrimaryKey(Fans record);
    //我的粉丝
    List<User> fans(Long userid);

    //我关注的人
    List<User> attention(Long userid);

}
