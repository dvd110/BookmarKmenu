package com.browser.bookmark.service;
import com.browser.bookmark.entity.Bookmark;
import com.browser.bookmark.entity.BookmarkExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: Wang
 * @Date: 2020/01/25 15:39
 * 功能描述:
 */

public interface IBookMarkService {

    //各种查 + 分页
    public List<Bookmark> BookmarkqueryAllList(Bookmark bookmark,int pageNo,int pageSize);
    long countByExample(Bookmark bookmark);

    int deleteByExample(BookmarkExample example);

    int deleteByPrimaryKey(Long bookmarkId);

    int insert(Bookmark record);

    int insertSelective(Bookmark record);

    List<Bookmark> selectByExample(BookmarkExample example);

    Bookmark selectByPrimaryKey(Long bookmarkId);

    int updateByExampleSelective(@Param("record") Bookmark record, @Param("example") BookmarkExample example);

    int updateByExample(@Param("record") Bookmark record, @Param("example") BookmarkExample example);

    int updateByPrimaryKeySelective(Bookmark record);

    int updateByPrimaryKey(Bookmark record);

    //发现功能
    List<Bookmark> selectBookmark();


}
