package com.browser.bookmark.service.impl;

import com.browser.bookmark.entity.*;
import com.browser.bookmark.maaper.FansMapper;
import com.browser.bookmark.service.IFansService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: Wang
 * @Date: 2020/02/17 22:35
 * 粉丝类
 */
@Service
public class FansServiceImpl implements IFansService {

    @Autowired
    private FansMapper fansMapper;

    /**
     * 查询数量
     * @param example
     */
    @Override
    public long countByExample(FansExample example) {

        return fansMapper.countByExample(example);
    }
    /**
     * 多条件删除功能
     * @param example
     */
    @Override
    public int deleteByExample(FansExample example) {
        return fansMapper.deleteByExample(example);
    }
    /**
     * 根据ID删除功能
     * @param id
     */
    @Override
    public int deleteByPrimaryKey(Long id) {
        return fansMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Fans record) {
        return 0;
    }
    /**
     * 添加功能
     * @param record
     */
    @Override
    public int insertSelective(Fans record) {
        return fansMapper.insertSelective(record);
    }

    @Override
    public List<Fans> selectByExample(FansExample example) {
        return null;
    }

    @Override
    public Fans selectByPrimaryKey(Long id) {
        return null;
    }

    @Override
    public int updateByExampleSelective(Fans record, FansExample example) {
        return 0;
    }

    @Override
    public int updateByExample(Fans record, FansExample example) {
        return 0;
    }
    /**
     * 添加功能
     * @param record
     */
    @Override
    public int updateByPrimaryKeySelective(Fans record) {
        return fansMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Fans record) {
        return 0;
    }

    /**
     * 我的粉丝
     * @param userid
     */
    @Override
    public List<User> fans(Long userid) {
        return fansMapper.fans(userid);
    }


    /**
     * 我关注的人
     * @param userid
     */
    @Override
    public List<User> attention(Long userid) {
        return fansMapper.attention(userid);
    }


}
