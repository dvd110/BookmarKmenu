package com.browser.bookmark.service.impl;

import com.browser.bookmark.entity.User;

import com.browser.bookmark.entity.UserExample;
import com.browser.bookmark.maaper.UserMapper;
import com.browser.bookmark.service.IUserService;

import com.browser.bookmark.utils.JWTUtil;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Auther: Wang
 * @Date: 2020/01/25 15:42
 * 功能描述:
 */
@Service
public class UserServiceImpl implements IUserService {

    @Autowired
   private UserMapper userMapper;
    /**
     * 根据用户名查询功能
     *
     * @param username
     */
    @Override
    public User selectUser(String username) {
        return userMapper.selectUser(username);
    }
    @Override
    public long countByExample(User user) {
        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        criteria.andUserNameEqualTo(user.getUserName());
        return userMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(UserExample example) {
        return 0;
    }
    /**
     * 根据ID删除
     *
     * @param userId
     */
    @Override
    public int deleteByPrimaryKey(Long userId) {
        return userMapper.deleteByPrimaryKey(userId);
    }

    @Override
    public int insert(User record) {
        return 0;
    }
    /**
     * 添加
     *
     * @param record
     */
    @Override
    public int insertSelective(User record) {
        return userMapper.insertSelective(record);
    }
    /**
     * 查询功能
     *
     * @param example
     */
    @Override
    public List<User> selectByExample(UserExample example) {
        return userMapper.selectByExample(example);
    }


    /**
     * ID查询功能
     *
     * @param userId
     */
    @Override
    public User selectByPrimaryKey(Long userId) {
        return userMapper.selectByPrimaryKey(userId);
    }

    @Override
    public int updateByExampleSelective(User record, UserExample example) {
        return 0;
    }

    @Override
    public int updateByExample(User record, UserExample example) {
        return 0;
    }
    /**
     * 修改功能
     *
     * @param record
     */
    @Override
    public int updateByPrimaryKeySelective(User record) {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= userMapper.selectUser(username).getUserId();
        record.setUserId(userid);
        return userMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(User record) {
        return 0;
    }




}
