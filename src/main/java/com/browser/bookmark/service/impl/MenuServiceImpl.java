package com.browser.bookmark.service.impl;

import com.browser.bookmark.entity.*;

import com.browser.bookmark.maaper.BookmarkMapper;
import com.browser.bookmark.maaper.MenuMapper;
import com.browser.bookmark.maaper.UserMapper;
import com.browser.bookmark.service.IBookMarkService;
import com.browser.bookmark.service.IMenuService;
import com.browser.bookmark.utils.JWTUtil;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * @Auther: Wang
 * @Date: 2020/01/25 15:44
 * 目录类
 */
@Service
public class MenuServiceImpl  implements IMenuService {

    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private BookmarkMapper BookmarkMapper;
    /**
     * 数量查询
     *
     * @param menu
     */
    @Override
    public long countByExample(Menu menu) {
        MenuExample example = new MenuExample();
        MenuExample.Criteria criteria = example.createCriteria();

        if(menu.getUserId()!= null && !"".equals(menu.getUserId())){
            criteria.andUserIdEqualTo(menu.getUserId());
        }
        if(menu.getMenuName()!= null && !"".equals(menu.getMenuName())){
            criteria.andMenuNameEqualTo(menu.getMenuName());
        }
        if(menu.getMenuId()!=null && !"".equals(menu.getMenuId())){
            criteria.andMenuIdEqualTo(menu.getMenuId());
        }
        if(menu.getParentId()!=null && !"".equals(menu.getParentId())){
            criteria.andParentIdEqualTo(menu.getParentId());
        }
        return menuMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(MenuExample example) {
        return 0;
    }
    /**
     * 根据ID删除
     *
     * @param menuId
     */

    @Override
    public int deleteByPrimaryKey(Long menuId) {
        //查询分类是userid
        Menu menu=menuMapper.selectByPrimaryKey(menuId);
         redisTemplate.delete("menu::"+menu.getUserId());
         //删除目录 该目录下书签移到回收站
        BookmarkMapper.updateIdelete(menuId);
        return menuMapper.deleteByPrimaryKey(menuId);
    }

    @Override
    public int insert(Menu record) {
        return 0;
    }

    /**
     * 添加目录
     *
     * @param record
     */
    @CacheEvict(value="menu",key="#record.userId")
    @Override
    public int insertSelective(Menu record) {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= userMapper.selectUser(username).getUserId();
        record.setUserId(userid);
        Menu me=new Menu();
        me.setMenuUrl("/bookmark");
        me.setUserId(userid);
        if (record.getMenuId()!=null){
            me.setParentId(record.getMenuId());
        }
        me.setMenuName(record.getMenuName());
        if (record.getMenuOrder()!=null){
        me.setMenuOrder(record.getMenuOrder());
        }
        return menuMapper.insertSelective(me);
    }
    /**
     * 查询目录
     *
     * @param menu
     */
    @Cacheable(value="menu",key="#menu.userId",condition = "#menu.parentId > 0")
    @Override
    public List<Menu> selectByExample(Menu menu) {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= userMapper.selectUser(username).getUserId();

        //redis缓存
       menu.setUserId(userid);
        MenuExample example = new MenuExample();
        MenuExample.Criteria criteria = example.createCriteria();
        example.setOrderByClause("menu_order asc");
        //通过用户名 查询 getUserID
//        if(user.getUserId()!=0){
//            criteria.andUserIdEqualTo(user.getUserId());
//        }
        if(menu.getUserId()!= null && !"".equals(menu.getUserId())){
            criteria.andUserIdEqualTo(menu.getUserId());
        }
        if(menu.getMenuName()!= null && !"".equals(menu.getMenuName())){
            criteria.andMenuNameEqualTo(menu.getMenuName());
        }
        if(menu.getParentId()!= null && !"".equals(menu.getParentId())){
            criteria.andParentIdEqualTo(menu.getParentId());
        }
        return menuMapper.selectByExample(example);
    }

    @Override
    public Menu selectByMenu(Menu menu) {


        return menuMapper.selectByMenu(menu);
    }

    /**
     * 根据ID查询目录
     *
     * @param menuId
     */
    @Override
    public Menu selectByPrimaryKey(Long menuId) {
        return menuMapper.selectByPrimaryKey(menuId);
    }

    @Override
    public int updateByExampleSelective(Menu record, MenuExample example) {
        return 0;
    }

    @Override
    public int updateByExample(Menu record, MenuExample example) {
        return 0;
    }

    /**
     * 修改功能
     *
     * @param record
     */
    @CacheEvict(value="menu",key="#record.userId")
    @Override
    public int updateByPrimaryKeySelective(Menu record) {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= userMapper.selectUser(username).getUserId();
        record.setUserId(userid);
        Menu menu= new Menu();
        if(record.getMenuName()!=null&& !"".equals(record.getMenuName())){
            menu.setMenuName(record.getMenuName());
        }
        menu.setMenuId(record.getMenuId());
        if(record.getMenuOrder()!=null&& !"".equals(record.getMenuOrder())){
            menu.setMenuOrder(record.getMenuOrder());
        }
        if (record.getParentId()!=null&& !"".equals(record.getParentId())&&!record.getParentId().equals(record.getMenuId())){
            menu.setParentId(record.getParentId());
        }
        return menuMapper.updateByPrimaryKeySelective(menu);
    }

    @Override
    public int updateByPrimaryKey(Menu record) {
        return 0;
    }
}
