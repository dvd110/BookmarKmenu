package com.browser.bookmark.service.impl;

import com.browser.bookmark.controller.BrowserController;
import com.browser.bookmark.entity.Bookmark;

import com.browser.bookmark.entity.BookmarkExample;
import com.browser.bookmark.entity.User;
import com.browser.bookmark.maaper.BookmarkMapper;

import com.browser.bookmark.maaper.UserMapper;
import com.browser.bookmark.service.IBookMarkService;
import com.browser.bookmark.utils.Base64;
import com.browser.bookmark.utils.JWTUtil;
import com.browser.bookmark.utils.RedisUtils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @Auther: Wang
 * @Date: 2020/01/25 15:40
 * 书签类
 */
@Service
public class BookMarkServiceImpl implements IBookMarkService {
    public static Logger logger = LoggerFactory.getLogger(BookMarkServiceImpl.class);
    @Autowired
    private BookmarkMapper bookMarkMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private UserMapper userMapper;


    /**
     * 查询功能
     *
     * @param bookmark
     * @param pageNo
     * @param pageSize
     */
    @Cacheable(value = "bookmark", key = "#bookmark.userid+'_'+#bookmark.menuId+'_'+#pageNo", condition = "(#bookmark.menuId > 0) and (#bookmark.userid>0)")
    @Override
    public List<Bookmark> BookmarkqueryAllList(Bookmark bookmark, int pageNo, int pageSize) {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid = userMapper.selectUser(username).getUserId();
        BookmarkExample example = new BookmarkExample();
        example.setOrderByClause("create_time desc");
        //分页
        example.setPageNo(pageNo);
        example.setPageSize(pageSize);

        BookmarkExample.Criteria criteria = example.createCriteria();
        criteria.andUseridEqualTo(userid);
        if (bookmark.getIdelete() != null && !"".equals(bookmark.getIdelete())) {
            criteria.andIdeleteEqualTo(bookmark.getIdelete());
        }
        if (bookmark.getUserid() != null && !"".equals(bookmark.getUserid())) {
            criteria.andUseridEqualTo(bookmark.getUserid());
        }
        if (bookmark.getMenuId() != null && !"".equals(bookmark.getMenuId())) {
            criteria.andMenuIdEqualTo(bookmark.getMenuId());
        }
        if (bookmark.getStart() != null && !"".equals(bookmark.getStart())) {
            criteria.andStartEqualTo(bookmark.getStart());
        }
        if (bookmark.getTitle() != null && !"".equals(bookmark.getTitle())) {
            String str = bookmark.getTitle();
            str = str.trim();
            criteria.andTitleLike("%" + str + "%");
        }
        List<Bookmark> lsit = bookMarkMapper.selectByExample(example);


        return lsit;
    }

    /**
     * 查询数量
     *
     * @param bookmark
     */
    @Override
    public long countByExample(Bookmark bookmark) {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid = userMapper.selectUser(username).getUserId();
        bookmark.setUserid(userid);
        BookmarkExample example = new BookmarkExample();
        BookmarkExample.Criteria criteria = example.createCriteria();
        if (bookmark.getUserid() != null && !"".equals(bookmark.getUserid())) {
            criteria.andUseridEqualTo(bookmark.getUserid());
        }
        if (bookmark.getMenuId() != null && !"".equals(bookmark.getMenuId())) {
            criteria.andMenuIdEqualTo(bookmark.getMenuId());
        }
        if (bookmark.getUrl() != null && !"".equals(bookmark.getUrl())) {
            criteria.andUrlEqualTo(bookmark.getUrl());
        }
        if (bookmark.getIdelete() != null && !"".equals(bookmark.getIdelete())) {
            criteria.andIdeleteEqualTo(bookmark.getIdelete());
        }
        if (bookmark.getStart() != null && !"".equals(bookmark.getStart())) {
            criteria.andStartEqualTo(bookmark.getStart());
        }
        return bookMarkMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(BookmarkExample example) {
        return 0;
    }

    /**
     * 根据ID删除
     *
     * @param bookmarkId
     */
    @Override
    public int deleteByPrimaryKey(Long bookmarkId) {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid = userMapper.selectUser(username).getUserId();
        //id查询目录目录id 删除缓存
        Bookmark bookmark = bookMarkMapper.selectByPrimaryKey(bookmarkId);
        Set<String> keys = redisTemplate.keys("bookmark::" + userid + '_' + bookmark.getMenuId() + "" + '_' + "?");
        redisTemplate.delete(keys);


        return bookMarkMapper.deleteByPrimaryKey(bookmarkId);
    }

    @Override
    public int insert(Bookmark record) {
        return 0;
    }

    /**
     * 添加功能
     *
     * @param record
     */

    @Override
    public int insertSelective(Bookmark record) {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid = userMapper.selectUser(username).getUserId();
        //删除缓存
        Set<String> keys = redisTemplate.keys("bookmark::" + userid + '_' + record.getMenuId() + "" + '_' + "?");
        redisTemplate.delete(keys);

        record.setUserid(userid);
        try {
            //获取主机
            record.setUrls(Base64.Urlutils(new URL(record.getUrl())));
            //ico转base64
            // record.setImage(Base64.encodeImageToBase64(new URL("https://"+record.getUrls()+"/favicon.ico")));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Date datee = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStr = format.format(datee);
        record.setCreateTime(dateStr);
        return bookMarkMapper.insertSelective(record);
    }

    /**
     * 查询功能
     *
     * @param example
     */
    @Override
    public List<Bookmark> selectByExample(BookmarkExample example) {
        return bookMarkMapper.selectByExample(example);
    }

    /**
     * 根据ID查询功能
     *
     * @param bookmarkId
     */
    @Override
    public Bookmark selectByPrimaryKey(Long bookmarkId) {
        return bookMarkMapper.selectByPrimaryKey(bookmarkId);
    }

    @Override
    public int updateByExampleSelective(Bookmark record, BookmarkExample example) {
        return 0;
    }

    @Override
    public int updateByExample(Bookmark record, BookmarkExample example) {
        return 0;
    }

    /**
     * 修改功能
     *
     * @param record
     */
    @CacheEvict(value = "bookmark", key = "#record.menuId+'_'+'*'")
    @Override
    public int updateByPrimaryKeySelective(Bookmark record) {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid = userMapper.selectUser(username).getUserId();
        //修改就是未删除状态
        record.setIdelete(0);
        //最新时间
        if(record.getZcount()!=null){
        Date datee = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStr = format.format(datee);
        record.setCreateTime(dateStr);
        }
        //删除缓存
        Set<String> keys = redisTemplate.keys("bookmark::" + userid + '_' + record.getMenuId() + "" + '_' + "?");
        redisTemplate.delete(keys);
        return bookMarkMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Bookmark record) {
        return 0;
    }

    /**
     * 发现功能
     */
    @Override
    public List<Bookmark> selectBookmark() {
        return bookMarkMapper.selectBookmark();
    }



}
