package com.browser.bookmark.service;

import com.browser.bookmark.entity.User;

/**
 * @Auther: Wang
 * @Date: 2020/02/20 13:45
 * 功能描述:
 */
public interface IMailService {

    /**
     * 发送文本邮件
     * @param to
     * @param subject
     * @param content
     */
    public void sendSimpleMail(String to, String subject, String content);

    public void sendSimpleMail(String to, String subject, String content, String... cc);

}