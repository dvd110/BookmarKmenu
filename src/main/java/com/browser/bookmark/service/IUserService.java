package com.browser.bookmark.service;

import com.browser.bookmark.entity.User;
import com.browser.bookmark.entity.UserExample;
import org.apache.ibatis.annotations.Param;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Auther: Wang
 * @Date: 2020/01/25 15:42
 * 功能描述:
 */
public interface IUserService {

    long countByExample(User user);

    int deleteByExample(UserExample example);

    int deleteByPrimaryKey(Long userId);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByExample(UserExample example);

    User selectUser(String username); //登录

    User selectByPrimaryKey(Long userId);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);


}
