package com.browser.bookmark.service;



import com.browser.bookmark.entity.Menu;
import com.browser.bookmark.entity.MenuExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: Wang
 * @Date: 2020/01/25 15:41
 * 功能描述:
 */
public interface IMenuService {


    long countByExample(Menu menu);

    int deleteByExample(MenuExample example);

    int deleteByPrimaryKey(Long menuId);

    int insert(Menu record);

    int insertSelective(Menu record);

    List<Menu> selectByExample(Menu menu);

    Menu selectByMenu(Menu menu);

    Menu selectByPrimaryKey(Long menuId);

    int updateByExampleSelective(@Param("record") Menu record, @Param("example") MenuExample example);

    int updateByExample(@Param("record") Menu record, @Param("example") MenuExample example);

    int updateByPrimaryKeySelective(Menu record);

    int updateByPrimaryKey(Menu record);

}
