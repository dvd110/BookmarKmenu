package com.browser.bookmark.utils;





import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @Auther: Wang
 * @Date: 2020/02/15 13:25
 * 功能描述:
 */
public class Base64 {


//    /**
//     * 将网络图片编码为base64
//     *
//     * @param url
//     * @return
//     * @throws
//     */
    public static String encodeImageToBase64(URL url) throws Exception {
//将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        //System.out.println("图片的路径为:" + url.toString());
//打开链接
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            //设置请求方式为"GET"
            conn.setRequestMethod("GET");
            //超时响应时间为5秒
            conn.setConnectTimeout(5 * 1000);
            //通过输入流获取图片数据
            InputStream inStream = conn.getInputStream();
            //得到图片的二进制数据，以二进制封装得到数据，具有通用性
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            //创建一个Buffer字符串
            byte[] buffer = new byte[1024];
            //每次读取的字符串长度，如果为-1，代表全部读取完毕
            int len = 0;
            //使用一个输入流从buffer里把数据读取出来
            while ((len = inStream.read(buffer)) != -1) {
            //用输出流往buffer里写入数据，中间参数代表从哪个位置开始读，len代表读取的长度
                outStream.write(buffer, 0, len);
            }
            //关闭输入流
            inStream.close();
            byte[] data = outStream.toByteArray();
            //对字节数组Base64编码
            BASE64Encoder encoder = new BASE64Encoder();
            String base64 = encoder.encode(data);
            System.out.println("网络文件[{"+url.toString()+"}]编码成base64字符串:[{"+base64+"}]");
            return base64;//返回Base64编码过的字节数组字符串
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception("图片上传失败,请联系客服!");
        }
    }
//    public static void main(String[] args) throws Exception {
//        String imgFilePath="https://www.zyymg.com/v2/get.php?url=https://www.cnblogs.com/chen1005/p/10481102.html";
//        encodeImageToBase64(new URL(imgFilePath));//将网络图片编码为base64
//
//    }


//    /**
//     * @auther: Wang
//     * @date: 2020/02/15 14:44
//     * 功能描述:分割书签URL 得到官网主机
//     * @return
//     */
    public static String Urlutils(URL url) throws MalformedURLException {
//        URL urls= new URL("http://blog.csdn.net/");
        String host = url.getHost();// 获取主机名
        //System.out.println("host:"+host);// 结果 blog.csdn.net
        return host;
    }
//
////    public static void main(String[] args) {
////        String imgFilePath="https://168ziyuan.cn/public/favicon.ico";
////        try {
////            String base64_str = Urlutils(new URL(imgFilePath));//将网络图片编码为base64
////        } catch (MalformedURLException e) {
////            e.printStackTrace();
////        }
////    }
//

}