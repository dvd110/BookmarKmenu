package com.browser.bookmark.IException;

import lombok.Data;

/**
 * 自定义业务异常
 */

public class ViewErrorException extends RuntimeException {
    private int code;
    private String message;

    public ViewErrorException(ErrorEnum errorEnum) {
        this.code = errorEnum.getCode();
        this.message = errorEnum.getMsg();
    }



    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}