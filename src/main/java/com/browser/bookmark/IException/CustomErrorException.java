package com.browser.bookmark.IException;

import lombok.Data;

/**
 * 自定义业务异常
 */

public class CustomErrorException extends RuntimeException {
    private int code;
    private String message;

    public CustomErrorException(ErrorEnum errorEnum) {
        this.code = errorEnum.getCode();
        this.message = errorEnum.getMsg();
    }
    public CustomErrorException(int code,String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}