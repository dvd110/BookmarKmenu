package com.browser.bookmark.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Bookmark implements Serializable {
    private Long bookmarkId;

    private Long userid;

    private String title;

    private String url;

    private String urls;

    private String description;

    private String label;

    private Long menuId;

    private Long zcount;

    private Integer idelete;

    private Integer start;
    @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss" )
    private String createTime;

    private String image;

    private List BookUser; //映射

    public List getBookUser() {
        return BookUser;
    }

    public void setBookUser(List bookUser) {
        BookUser = bookUser;
    }

    private static final long serialVersionUID = 1L;

    public Long getBookmarkId() {
        return bookmarkId;
    }

    public void setBookmarkId(Long bookmarkId) {
        this.bookmarkId = bookmarkId;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrls() {
        return urls;
    }

    public void setUrls(String urls) {
        this.urls = urls;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public Long getZcount() {
        return zcount;
    }

    public void setZcount(Long zcount) {
        this.zcount = zcount;
    }

    public Integer getIdelete() {
        return idelete;
    }

    public void setIdelete(Integer idelete) {
        this.idelete = idelete;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}