package com.browser.bookmark.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookmarkExample {
    protected Integer pageNo;
    protected Integer pageSize;

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BookmarkExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andBookmarkIdIsNull() {
            addCriterion("bookmark_id is null");
            return (Criteria) this;
        }

        public Criteria andBookmarkIdIsNotNull() {
            addCriterion("bookmark_id is not null");
            return (Criteria) this;
        }

        public Criteria andBookmarkIdEqualTo(Long value) {
            addCriterion("bookmark_id =", value, "bookmarkId");
            return (Criteria) this;
        }

        public Criteria andBookmarkIdNotEqualTo(Long value) {
            addCriterion("bookmark_id <>", value, "bookmarkId");
            return (Criteria) this;
        }

        public Criteria andBookmarkIdGreaterThan(Long value) {
            addCriterion("bookmark_id >", value, "bookmarkId");
            return (Criteria) this;
        }

        public Criteria andBookmarkIdGreaterThanOrEqualTo(Long value) {
            addCriterion("bookmark_id >=", value, "bookmarkId");
            return (Criteria) this;
        }

        public Criteria andBookmarkIdLessThan(Long value) {
            addCriterion("bookmark_id <", value, "bookmarkId");
            return (Criteria) this;
        }

        public Criteria andBookmarkIdLessThanOrEqualTo(Long value) {
            addCriterion("bookmark_id <=", value, "bookmarkId");
            return (Criteria) this;
        }

        public Criteria andBookmarkIdIn(List<Long> values) {
            addCriterion("bookmark_id in", values, "bookmarkId");
            return (Criteria) this;
        }

        public Criteria andBookmarkIdNotIn(List<Long> values) {
            addCriterion("bookmark_id not in", values, "bookmarkId");
            return (Criteria) this;
        }

        public Criteria andBookmarkIdBetween(Long value1, Long value2) {
            addCriterion("bookmark_id between", value1, value2, "bookmarkId");
            return (Criteria) this;
        }

        public Criteria andBookmarkIdNotBetween(Long value1, Long value2) {
            addCriterion("bookmark_id not between", value1, value2, "bookmarkId");
            return (Criteria) this;
        }

        public Criteria andUseridIsNull() {
            addCriterion("userid is null");
            return (Criteria) this;
        }

        public Criteria andUseridIsNotNull() {
            addCriterion("userid is not null");
            return (Criteria) this;
        }

        public Criteria andUseridEqualTo(Long value) {
            addCriterion("userid =", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotEqualTo(Long value) {
            addCriterion("userid <>", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThan(Long value) {
            addCriterion("userid >", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThanOrEqualTo(Long value) {
            addCriterion("userid >=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThan(Long value) {
            addCriterion("userid <", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThanOrEqualTo(Long value) {
            addCriterion("userid <=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridIn(List<Long> values) {
            addCriterion("userid in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotIn(List<Long> values) {
            addCriterion("userid not in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridBetween(Long value1, Long value2) {
            addCriterion("userid between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotBetween(Long value1, Long value2) {
            addCriterion("userid not between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andTitleIsNull() {
            addCriterion("title is null");
            return (Criteria) this;
        }

        public Criteria andTitleIsNotNull() {
            addCriterion("title is not null");
            return (Criteria) this;
        }

        public Criteria andTitleEqualTo(String value) {
            addCriterion("title =", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotEqualTo(String value) {
            addCriterion("title <>", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThan(String value) {
            addCriterion("title >", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThanOrEqualTo(String value) {
            addCriterion("title >=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThan(String value) {
            addCriterion("title <", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThanOrEqualTo(String value) {
            addCriterion("title <=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLike(String value) {
            addCriterion("title like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotLike(String value) {
            addCriterion("title not like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleIn(List<String> values) {
            addCriterion("title in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotIn(List<String> values) {
            addCriterion("title not in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleBetween(String value1, String value2) {
            addCriterion("title between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotBetween(String value1, String value2) {
            addCriterion("title not between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andUrlIsNull() {
            addCriterion("url is null");
            return (Criteria) this;
        }

        public Criteria andUrlIsNotNull() {
            addCriterion("url is not null");
            return (Criteria) this;
        }

        public Criteria andUrlEqualTo(String value) {
            addCriterion("url =", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotEqualTo(String value) {
            addCriterion("url <>", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlGreaterThan(String value) {
            addCriterion("url >", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlGreaterThanOrEqualTo(String value) {
            addCriterion("url >=", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlLessThan(String value) {
            addCriterion("url <", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlLessThanOrEqualTo(String value) {
            addCriterion("url <=", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlLike(String value) {
            addCriterion("url like", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotLike(String value) {
            addCriterion("url not like", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlIn(List<String> values) {
            addCriterion("url in", values, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotIn(List<String> values) {
            addCriterion("url not in", values, "url");
            return (Criteria) this;
        }

        public Criteria andUrlBetween(String value1, String value2) {
            addCriterion("url between", value1, value2, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotBetween(String value1, String value2) {
            addCriterion("url not between", value1, value2, "url");
            return (Criteria) this;
        }

        public Criteria andUrlsIsNull() {
            addCriterion("urls is null");
            return (Criteria) this;
        }

        public Criteria andUrlsIsNotNull() {
            addCriterion("urls is not null");
            return (Criteria) this;
        }

        public Criteria andUrlsEqualTo(String value) {
            addCriterion("urls =", value, "urls");
            return (Criteria) this;
        }

        public Criteria andUrlsNotEqualTo(String value) {
            addCriterion("urls <>", value, "urls");
            return (Criteria) this;
        }

        public Criteria andUrlsGreaterThan(String value) {
            addCriterion("urls >", value, "urls");
            return (Criteria) this;
        }

        public Criteria andUrlsGreaterThanOrEqualTo(String value) {
            addCriterion("urls >=", value, "urls");
            return (Criteria) this;
        }

        public Criteria andUrlsLessThan(String value) {
            addCriterion("urls <", value, "urls");
            return (Criteria) this;
        }

        public Criteria andUrlsLessThanOrEqualTo(String value) {
            addCriterion("urls <=", value, "urls");
            return (Criteria) this;
        }

        public Criteria andUrlsLike(String value) {
            addCriterion("urls like", value, "urls");
            return (Criteria) this;
        }

        public Criteria andUrlsNotLike(String value) {
            addCriterion("urls not like", value, "urls");
            return (Criteria) this;
        }

        public Criteria andUrlsIn(List<String> values) {
            addCriterion("urls in", values, "urls");
            return (Criteria) this;
        }

        public Criteria andUrlsNotIn(List<String> values) {
            addCriterion("urls not in", values, "urls");
            return (Criteria) this;
        }

        public Criteria andUrlsBetween(String value1, String value2) {
            addCriterion("urls between", value1, value2, "urls");
            return (Criteria) this;
        }

        public Criteria andUrlsNotBetween(String value1, String value2) {
            addCriterion("urls not between", value1, value2, "urls");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNull() {
            addCriterion("description is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("description is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("description =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("description <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("description >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("description >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("description <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("description <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("description like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("description not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("description in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("description not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("description between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("description not between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andLabelIsNull() {
            addCriterion("Label is null");
            return (Criteria) this;
        }

        public Criteria andLabelIsNotNull() {
            addCriterion("Label is not null");
            return (Criteria) this;
        }

        public Criteria andLabelEqualTo(String value) {
            addCriterion("Label =", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelNotEqualTo(String value) {
            addCriterion("Label <>", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelGreaterThan(String value) {
            addCriterion("Label >", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelGreaterThanOrEqualTo(String value) {
            addCriterion("Label >=", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelLessThan(String value) {
            addCriterion("Label <", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelLessThanOrEqualTo(String value) {
            addCriterion("Label <=", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelLike(String value) {
            addCriterion("Label like", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelNotLike(String value) {
            addCriterion("Label not like", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelIn(List<String> values) {
            addCriterion("Label in", values, "label");
            return (Criteria) this;
        }

        public Criteria andLabelNotIn(List<String> values) {
            addCriterion("Label not in", values, "label");
            return (Criteria) this;
        }

        public Criteria andLabelBetween(String value1, String value2) {
            addCriterion("Label between", value1, value2, "label");
            return (Criteria) this;
        }

        public Criteria andLabelNotBetween(String value1, String value2) {
            addCriterion("Label not between", value1, value2, "label");
            return (Criteria) this;
        }

        public Criteria andMenuIdIsNull() {
            addCriterion("menu_id is null");
            return (Criteria) this;
        }

        public Criteria andMenuIdIsNotNull() {
            addCriterion("menu_id is not null");
            return (Criteria) this;
        }

        public Criteria andMenuIdEqualTo(Long value) {
            addCriterion("menu_id =", value, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdNotEqualTo(Long value) {
            addCriterion("menu_id <>", value, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdGreaterThan(Long value) {
            addCriterion("menu_id >", value, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdGreaterThanOrEqualTo(Long value) {
            addCriterion("menu_id >=", value, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdLessThan(Long value) {
            addCriterion("menu_id <", value, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdLessThanOrEqualTo(Long value) {
            addCriterion("menu_id <=", value, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdIn(List<Long> values) {
            addCriterion("menu_id in", values, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdNotIn(List<Long> values) {
            addCriterion("menu_id not in", values, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdBetween(Long value1, Long value2) {
            addCriterion("menu_id between", value1, value2, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdNotBetween(Long value1, Long value2) {
            addCriterion("menu_id not between", value1, value2, "menuId");
            return (Criteria) this;
        }

        public Criteria andZcountIsNull() {
            addCriterion("zcount is null");
            return (Criteria) this;
        }

        public Criteria andZcountIsNotNull() {
            addCriterion("zcount is not null");
            return (Criteria) this;
        }

        public Criteria andZcountEqualTo(Long value) {
            addCriterion("zcount =", value, "zcount");
            return (Criteria) this;
        }

        public Criteria andZcountNotEqualTo(Long value) {
            addCriterion("zcount <>", value, "zcount");
            return (Criteria) this;
        }

        public Criteria andZcountGreaterThan(Long value) {
            addCriterion("zcount >", value, "zcount");
            return (Criteria) this;
        }

        public Criteria andZcountGreaterThanOrEqualTo(Long value) {
            addCriterion("zcount >=", value, "zcount");
            return (Criteria) this;
        }

        public Criteria andZcountLessThan(Long value) {
            addCriterion("zcount <", value, "zcount");
            return (Criteria) this;
        }

        public Criteria andZcountLessThanOrEqualTo(Long value) {
            addCriterion("zcount <=", value, "zcount");
            return (Criteria) this;
        }

        public Criteria andZcountIn(List<Long> values) {
            addCriterion("zcount in", values, "zcount");
            return (Criteria) this;
        }

        public Criteria andZcountNotIn(List<Long> values) {
            addCriterion("zcount not in", values, "zcount");
            return (Criteria) this;
        }

        public Criteria andZcountBetween(Long value1, Long value2) {
            addCriterion("zcount between", value1, value2, "zcount");
            return (Criteria) this;
        }

        public Criteria andZcountNotBetween(Long value1, Long value2) {
            addCriterion("zcount not between", value1, value2, "zcount");
            return (Criteria) this;
        }

        public Criteria andIdeleteIsNull() {
            addCriterion("IDelete is null");
            return (Criteria) this;
        }

        public Criteria andIdeleteIsNotNull() {
            addCriterion("IDelete is not null");
            return (Criteria) this;
        }

        public Criteria andIdeleteEqualTo(Integer value) {
            addCriterion("IDelete =", value, "idelete");
            return (Criteria) this;
        }

        public Criteria andIdeleteNotEqualTo(Integer value) {
            addCriterion("IDelete <>", value, "idelete");
            return (Criteria) this;
        }

        public Criteria andIdeleteGreaterThan(Integer value) {
            addCriterion("IDelete >", value, "idelete");
            return (Criteria) this;
        }

        public Criteria andIdeleteGreaterThanOrEqualTo(Integer value) {
            addCriterion("IDelete >=", value, "idelete");
            return (Criteria) this;
        }

        public Criteria andIdeleteLessThan(Integer value) {
            addCriterion("IDelete <", value, "idelete");
            return (Criteria) this;
        }

        public Criteria andIdeleteLessThanOrEqualTo(Integer value) {
            addCriterion("IDelete <=", value, "idelete");
            return (Criteria) this;
        }

        public Criteria andIdeleteIn(List<Integer> values) {
            addCriterion("IDelete in", values, "idelete");
            return (Criteria) this;
        }

        public Criteria andIdeleteNotIn(List<Integer> values) {
            addCriterion("IDelete not in", values, "idelete");
            return (Criteria) this;
        }

        public Criteria andIdeleteBetween(Integer value1, Integer value2) {
            addCriterion("IDelete between", value1, value2, "idelete");
            return (Criteria) this;
        }

        public Criteria andIdeleteNotBetween(Integer value1, Integer value2) {
            addCriterion("IDelete not between", value1, value2, "idelete");
            return (Criteria) this;
        }

        public Criteria andStartIsNull() {
            addCriterion("Start is null");
            return (Criteria) this;
        }

        public Criteria andStartIsNotNull() {
            addCriterion("Start is not null");
            return (Criteria) this;
        }

        public Criteria andStartEqualTo(Integer value) {
            addCriterion("Start =", value, "start");
            return (Criteria) this;
        }

        public Criteria andStartNotEqualTo(Integer value) {
            addCriterion("Start <>", value, "start");
            return (Criteria) this;
        }

        public Criteria andStartGreaterThan(Integer value) {
            addCriterion("Start >", value, "start");
            return (Criteria) this;
        }

        public Criteria andStartGreaterThanOrEqualTo(Integer value) {
            addCriterion("Start >=", value, "start");
            return (Criteria) this;
        }

        public Criteria andStartLessThan(Integer value) {
            addCriterion("Start <", value, "start");
            return (Criteria) this;
        }

        public Criteria andStartLessThanOrEqualTo(Integer value) {
            addCriterion("Start <=", value, "start");
            return (Criteria) this;
        }

        public Criteria andStartIn(List<Integer> values) {
            addCriterion("Start in", values, "start");
            return (Criteria) this;
        }

        public Criteria andStartNotIn(List<Integer> values) {
            addCriterion("Start not in", values, "start");
            return (Criteria) this;
        }

        public Criteria andStartBetween(Integer value1, Integer value2) {
            addCriterion("Start between", value1, value2, "start");
            return (Criteria) this;
        }

        public Criteria andStartNotBetween(Integer value1, Integer value2) {
            addCriterion("Start not between", value1, value2, "start");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}