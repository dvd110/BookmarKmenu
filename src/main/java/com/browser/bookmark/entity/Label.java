package com.browser.bookmark.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Label implements Serializable {
    private Long labelId;

    private Integer labelUserid;

    private String labelName;

    private Integer labelNum;

    public Long getLabelId() {
        return labelId;
    }

    public void setLabelId(Long labelId) {
        this.labelId = labelId;
    }

    public Integer getLabelUserid() {
        return labelUserid;
    }

    public void setLabelUserid(Integer labelUserid) {
        this.labelUserid = labelUserid;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public Integer getLabelNum() {
        return labelNum;
    }

    public void setLabelNum(Integer labelNum) {
        this.labelNum = labelNum;
    }
}