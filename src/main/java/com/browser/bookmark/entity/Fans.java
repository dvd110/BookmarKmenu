package com.browser.bookmark.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @auther: Wang
 * @date: 2020/02/17 22:31
 * 功能描述:粉丝 关注表
 */
public class Fans implements Serializable {
    private Long id;

    private Long userId;

    private Long fansUserId;

    private Date createTime;
    private List fansUser;

    public List getFansUser() {
        return fansUser;
    }

    public void setFansUser(List fansUser) {
        this.fansUser = fansUser;
    }

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFansUserId() {
        return fansUserId;
    }

    public void setFansUserId(Long fansUserId) {
        this.fansUserId = fansUserId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}