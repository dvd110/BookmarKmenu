package com.browser.bookmark.maaper;

import com.browser.bookmark.entity.Bookmark;
import com.browser.bookmark.entity.BookmarkExample;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface BookmarkMapper {
    long countByExample(BookmarkExample example);

    int deleteByExample(BookmarkExample example);

    int deleteByPrimaryKey(Long bookmarkId);

    int insert(Bookmark record);

    int insertSelective(Bookmark record);

    List<Bookmark> selectByExampleWithBLOBs(BookmarkExample example);

    List<Bookmark> selectByExample(BookmarkExample example);

    Bookmark selectByPrimaryKey(Long bookmarkId);

    int updateByExampleSelective(@Param("record") Bookmark record, @Param("example") BookmarkExample example);

    int updateByExampleWithBLOBs(@Param("record") Bookmark record, @Param("example") BookmarkExample example);

    int updateByExample(@Param("record") Bookmark record, @Param("example") BookmarkExample example);

    int updateByPrimaryKeySelective(Bookmark record);
    //删除目录 此目录下书签就移动到回收站
    int updateIdelete(Long menuId);
    int updateByPrimaryKeyWithBLOBs(Bookmark record);

    int updateByPrimaryKey(Bookmark record);

    //发现功能
    List<Bookmark> selectBookmark();
    //查询用户的所有公开书签
    List<Bookmark> userquerally(BookmarkExample example);
}