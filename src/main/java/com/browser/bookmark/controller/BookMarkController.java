package com.browser.bookmark.controller;

import com.browser.bookmark.entity.*;
import com.browser.bookmark.maaper.UserMapper;
import com.browser.bookmark.service.IFansService;
import com.browser.bookmark.service.IUserService;
import com.browser.bookmark.utils.JWTUtil;
import com.browser.bookmark.utils.RespBean;
import com.browser.bookmark.service.IBookMarkService;
import com.browser.bookmark.service.IMenuService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Auther: Wang
 * @Date: 2020/01/25 16:57
 */
@Api(description = "BookMark书签接口")
@RestController
@RequiresPermissions("user:show")
@RequestMapping("/bookmark")
public class BookMarkController {
    @Autowired
    private IMenuService menuService;

    @Autowired
    private IBookMarkService iBookMarkService;

    @Autowired
    private IUserService iUserService;
    @ApiOperation(value = "多条件书签查询")

    @GetMapping("/queryAllList")
    @ResponseBody
    public List<Bookmark> BookmarkqueryAll(Bookmark bookmark, @RequestParam(defaultValue = "0") int pageNo, @RequestParam(defaultValue = "99999") int pageSize) {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= iUserService.selectUser(username).getUserId();
        //查询某用户公开书签判断
        if (bookmark.getUserid()==null){
        bookmark.setUserid(userid);
        }
        List<Bookmark> list = iBookMarkService.BookmarkqueryAllList(bookmark, pageNo, pageSize);
        return list;
    }


    @ApiOperation(value = "根据ID查询")
    @ResponseBody
    @GetMapping("/selectByPrimaryKey/{bookmarkId}")
    public Bookmark selectByPrimaryKey(@PathVariable("bookmarkId") Long bookmarkId) {
        return iBookMarkService.selectByPrimaryKey(bookmarkId);
    }


    @ApiOperation(value = "根据ID删除")
    @GetMapping("/deleteByPrimaryKey/{bookmarkId}")
//    @CacheEvict(value="bookmark",key="#bookmark.bookmarkUserid+#bookmark.srotId")
    public RespBean deleteBookmark(@PathVariable("bookmarkId") Long bookmarkId) {

        int i = iBookMarkService.deleteByPrimaryKey(bookmarkId);
        if (i == 1) {
            return new RespBean("success", "删除成功!");
        }
        return new RespBean("error", "删除失败!");
    }

    @ApiOperation(value = "添加功能")
    @PostMapping("/insertSelective")
//    @CachePut(cacheNames = "bookmark",key = "#bookmark.bookmarkUserid+'_'+#bookmark.srotId")
    public RespBean addBookmark(@RequestBody Bookmark bookmark) {
        Bookmark list = new Bookmark();
        list.setTitle(bookmark.getTitle());
        List<Bookmark> lists = iBookMarkService.BookmarkqueryAllList(list, 0, 0);
        if (lists.size() == 0) {
            int i = iBookMarkService.insertSelective(bookmark);
            if (i == 1) {
                return new RespBean("success", "添加成功!");
            }
            return new RespBean("error", "添加失败!");
        } else {
            return new RespBean("errors", "本书签已经存在!");
        }
    }

    @ApiOperation(value = "修改功能")
    // @RequiresPermissions("user:admin")
    @PostMapping("/updateBookMark")
    public RespBean updateBookmark(@RequestBody Bookmark bookmark) {
        int i;
        //修改前先查询添加的到的目录是不是最下级目录
        Menu menu = new Menu();
        menu.setUserId(bookmark.getUserid());
        menu.setParentId(bookmark.getMenuId());
       long count= menuService.countByExample(menu);

        //是最下级目录 可以修改
        if (count == 0) {
            i = iBookMarkService.updateByPrimaryKeySelective(bookmark);
            if (i == 1) {
                return new RespBean("success", "修改成功!");
            }
        } else {
            return new RespBean("1000015", "请选择最下级目录，不要选择节点");
        }
        return new RespBean("error", "修改失败!");
    }


    @ApiOperation(value = "移除稍后再看")
    @GetMapping("/updateBookmarkStart")
    public RespBean updateBookmarkStart(Bookmark bookmark) {
        bookmark.setStart(0);
        int i = iBookMarkService.updateByPrimaryKeySelective(bookmark);
        if (i == 1) {
            return new RespBean("success", "此书签被打开,移除稍后再看,并且公开显示!");
        }
        return new RespBean("error", "修改失败!");
    }


    @ApiOperation(value = "发现功能 显示最新的20条收藏")
    @GetMapping("/discover")
    @ResponseBody
    public List<Bookmark> discover() {

        return iBookMarkService.selectBookmark();
    }


    @ApiOperation(value = "查询当前目录的记录数")
    @GetMapping("/countMenuId")
    @ResponseBody
    public Long countMenuId(Bookmark bookmark) {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= iUserService.selectUser(username).getUserId();
        //查询个人资料书签
        if(bookmark.getUserid()==null){
        bookmark.setUserid(userid);
        }
        return iBookMarkService.countByExample(bookmark);
    }
    @ApiOperation(value = "点赞+1")
    @GetMapping("/Zcount")
    @ResponseBody
    public RespBean Zcount(Bookmark bookmark) {

        Long book = iBookMarkService.selectByPrimaryKey(bookmark.getBookmarkId()).getZcount();
        bookmark.setZcount(book+1);
        System.out.println("当前点赞数是:"+book);
        int i=iBookMarkService.updateByPrimaryKeySelective(bookmark);
        if (i == 1) {
            return new RespBean("success", "点赞成功!");
        } else {
            return new RespBean("error", "点赞失败!");
        }
    }



}






