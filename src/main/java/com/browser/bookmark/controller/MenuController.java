package com.browser.bookmark.controller;

import com.browser.bookmark.entity.Menu;
import com.browser.bookmark.entity.User;
import com.browser.bookmark.service.IUserService;
import com.browser.bookmark.utils.JWTUtil;
import com.browser.bookmark.utils.RespBean;
import com.browser.bookmark.service.IMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Auther: Wang
 * @Date: 2020/02/05 11:16
 */
@Api(description = "书签目录接口")
@RestController
@RequiresPermissions("user:show")
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private IMenuService menuService;
    @Autowired
    private IUserService iUserService;

    @ApiOperation(value = "查询个人菜单目录")
    @GetMapping("/queryAllList")
    public List<Menu> BookmarkqueryAll(Menu menu) {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= iUserService.selectUser(username).getUserId();
        menu.setUserId(userid);
        List<Menu> list = menuService.selectByExample(menu);
        return list;
    }

    @ApiOperation(value = "根据ID删除目录")
    @GetMapping("/deleteByPrimaryKey/{menuId}")
    public RespBean deleteBookmark(@PathVariable("menuId") Long menuId) {
        int i = menuService.deleteByPrimaryKey(menuId);
        if (i == 1) {
            return new RespBean("success", "删除成功!");
        }
        return new RespBean("error", "删除失败!");
    }

    @ApiOperation(value = "添加书签目录")
    @PostMapping("/insertSelective")
    public RespBean addBookmark(@RequestBody Menu menu) {
        int i = menuService.insertSelective(menu);
        if (i == 1) {
            return new RespBean("success", "添加成功!");
        }
        return new RespBean("error", "添加失败!");
    }

    @ApiOperation(value = "修改书签目录")
    @PostMapping("/updateMenu")
    public RespBean updateBookmark(@RequestBody Menu menu) {
        int i = menuService.updateByPrimaryKeySelective(menu);
        if (i == 1) {
            return new RespBean("success", "修改成功!");
        }
        return new RespBean("error", "修改失败!");
    }


    @ApiOperation(value = "ID查询信息 目录名字")
    @GetMapping("/bookMakeName")
    @ResponseBody
    public Menu bookMakeName(Long menuId) {
        return menuService.selectByPrimaryKey(menuId);
    }


}
