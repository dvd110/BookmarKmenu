package com.browser.bookmark.controller;

import com.browser.bookmark.entity.*;
import com.browser.bookmark.maaper.UserMapper;
import com.browser.bookmark.service.IBookMarkService;
import com.browser.bookmark.service.IFansService;
import com.browser.bookmark.service.IUserService;
import com.browser.bookmark.utils.JWTUtil;
import com.browser.bookmark.utils.RespBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: Wang
 * @Date: 2020/02/17 23:00
 */
@Api(description = "粉丝相关接口")
@Controller
@RequiresPermissions("user:show")
@RequestMapping("/fans")
public class FansController {

    @Autowired
    private IFansService iFansService;

    @Autowired
    private IUserService iUserService;

    @Autowired
    private IBookMarkService iBookMarkService;




    @ApiOperation(value = "我的粉丝列表")
    @GetMapping("/myfans")
    @ResponseBody
    public List<User> myfans() {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= iUserService.selectUser(username).getUserId();
        return iFansService.fans(userid);
    }


    @ApiOperation(value = "我关注的人列表")
    @GetMapping("/attention")
    @ResponseBody
    public List<User> attention() {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= iUserService.selectUser(username).getUserId();
        return iFansService.attention(userid);
    }


    @ApiOperation(value = "我的粉丝数")
    @GetMapping("/countfans")
    @ResponseBody
    public Long countfans() {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= iUserService.selectUser(username).getUserId();
        FansExample example = new FansExample();
        FansExample.Criteria criteria = example.createCriteria();
        criteria.andFansUserIdEqualTo(userid);
        return iFansService.countByExample(example);
    }


    @ApiOperation(value = "我关注人数")
    @GetMapping("/countattention")
    @ResponseBody
    public Long countdis() {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= iUserService.selectUser(username).getUserId();
        FansExample example = new FansExample();
        FansExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userid);
        return iFansService.countByExample(example);
    }


    @ApiOperation(value = "我的收藏数量")
    @GetMapping("/countBookMark")
    @ResponseBody
    public Long countattention() {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= iUserService.selectUser(username).getUserId();
        Bookmark bookmark = new Bookmark();
        bookmark.setUserid(userid);
        return iBookMarkService.countByExample(bookmark);
    }


    @ApiOperation(value = "关注TA人")
    @PostMapping("/insertfans")
    public RespBean insertfans(@RequestBody Fans fans) {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= iUserService.selectUser(username).getUserId();
        fans.setUserId(userid);
        int i = iFansService.insertSelective(fans);
        if (i == 1) {
            return new RespBean("success", "关注成功!");
        } else {
            return new RespBean("error", "关注失败!");
        }
    }


    @ApiOperation(value = "取消关注TA人")
    @GetMapping("/deleteFans")
    @ResponseBody
    public RespBean deleteFans(Fans fans) {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= iUserService.selectUser(username).getUserId();
        FansExample example = new FansExample();
        FansExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userid);
        criteria.andFansUserIdEqualTo(fans.getFansUserId());
        int i = iFansService.deleteByExample(example);
        if (i == 1) {
            return new RespBean("success", "取消成功!");
        } else {
            return new RespBean("error", "取消失败!");
        }
    }

}
