package com.browser.bookmark.controller;


import com.browser.bookmark.entity.User;
import com.browser.bookmark.entity.UserExample;
import com.browser.bookmark.utils.JWTUtil;
import com.browser.bookmark.utils.RedisUtils;
import com.browser.bookmark.utils.RespBean;
import com.browser.bookmark.service.IUserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
//import com.eigpay.supp.general.admin.manager.security.UsernamePasswordToken;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;


import java.util.List;


/**
 * @Auther: Wang
 * @Date: 2020/01/25 16:57
 * 功能描述:
 */

@Api(description = "后台-用户表操作接口")
@RestController
@RequiresPermissions("user:show")
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService iUserService;


    @ApiOperation(value = "后台条件查询用户信息")
    //  @RequiresPermissions("user:admin")
    @GetMapping("/queryAllList")
    public PageInfo<User> UserqueryAll(User user, @RequestParam(defaultValue = "0") int pageNo, @RequestParam(defaultValue = "50") int pageSize) {

        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();

        if (user.getUserName() != null && !"".equals(user.getUserName())) {
            criteria.andUserNameEqualTo(user.getUserName());
        }

        if (user.getPhon() != null && !"".equals(user.getPhon())) {
            criteria.andPhonEqualTo(user.getPhon());
        }

        if (user.getEmail() != null && !"".equals(user.getEmail())) {
            criteria.andEmailEqualTo(user.getEmail());
        }
        //只会分页下面的第一个查询语句
        PageHelper.startPage(pageNo, pageSize);
        List<User> list = iUserService.selectByExample(example);
        PageInfo<User> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }


    @ApiOperation(value = "后台通过id查询用户信息")
    @GetMapping("/UserAll")
    public User UserAll() {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= iUserService.selectUser(username).getUserId();
        return iUserService.selectByPrimaryKey(userid);
    }


    @ApiOperation(value = "后台根据ID删除用户")
//    @RequiresPermissions("user:admin")
    @GetMapping("/deleteByPrimaryKey/{userId}")
    public RespBean deleteBookmark(@PathVariable("userId") Long userId) {
        int i = iUserService.deleteByPrimaryKey(userId);
        if (i == 1) {
            return new RespBean("success", "删除成功!");
        }
        return new RespBean("error", "删除失败!");
    }


    @ApiOperation(value = "后台直接添加用户用户")
    @RequiresPermissions("user:admin")
    @PostMapping("/insertSelective")
    public RespBean addBookmark(@RequestBody User user) {

        int i = iUserService.insertSelective(user);
        if (i == 1) {
            return new RespBean("success", "添加成功!");
        }
        return new RespBean("error", "添加失败!");
    }


    @ApiOperation(value = "后台修改用户")
    @PostMapping("/updateUser")
    public RespBean updateBookmark(@RequestBody User user) {
        int i = iUserService.updateByPrimaryKeySelective(user);
        if (i == 1) {
            return new RespBean("success", "修改成功!");
        }
        return new RespBean("error", "修改失败!");
    }




}






