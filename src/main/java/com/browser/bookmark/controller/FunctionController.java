package com.browser.bookmark.controller;

import com.browser.bookmark.entity.*;
import com.browser.bookmark.maaper.UserMapper;
import com.browser.bookmark.service.IBookMarkService;
import com.browser.bookmark.service.IFansService;
import com.browser.bookmark.service.IUserService;
import com.browser.bookmark.utils.JWTUtil;
import com.browser.bookmark.utils.RespBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

/**
 * @Auther: Wang
 * @Date: 2020/02/17 21:22
 */
@Api(description = "扩展功能口")
@RestController
@RequiresPermissions("user:show")
@RequestMapping("/function")
public class FunctionController {

    @Autowired
    private IBookMarkService iBookMarkService;
    @Autowired
    private IUserService iUserService;

    @ApiOperation(value = "稍后再看功能")
    @GetMapping("/minute")
    public List<Bookmark> minute() {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= iUserService.selectUser(username).getUserId();
        BookmarkExample example = new BookmarkExample();
        BookmarkExample.Criteria criteria = example.createCriteria();
        criteria.andStartEqualTo(3);
        criteria.andUseridEqualTo(userid);
        return iBookMarkService.selectByExample(example);
    }


    @ApiOperation(value = "稍后再看 看后移除")
    @GetMapping("/minuteupdate")
    public RespBean minuteupdate(@RequestBody Bookmark bookmark) {
        String username = JWTUtil.getUsername((String) SecurityUtils.getSubject().getPrincipal());
        Long userid= iUserService.selectUser(username).getUserId();
        bookmark.setUserid(userid);
        int i = iBookMarkService.updateByPrimaryKeySelective(bookmark);
        if (i == 1) {
            return new RespBean("success", "修改成功!");
        } else {
            return new RespBean("error", "修改失败!");
        }
    }


}
